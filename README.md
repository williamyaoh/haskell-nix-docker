# haskell-docker-nix

Every build system in existence apparently needs a Docker image, so here's
a Dockerfile that correctly sets up a Nix environment with GHC 8.4.4 installed.
