FROM nixos/nix:2.0.4

# LANG information isn't set in a pure Nix container
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Annoyingly, we have to manually add in Certificate Authority certificates
# to our image so that we can do... anything online.
ADD ./digicert.crt digicert.crt
RUN apk update
RUN apk add ca-certificates
RUN update-ca-certificates digicert.crt

RUN nix-channel --add https://nixos.org/channels/nixos-18.09 nixpkgs
RUN nix-channel --update
RUN nix-env -f '<nixpkgs>' -iA stack haskell.compiler.ghc822

# Here we set our default Stackage resolver and enable Nix for Stack builds.
# Change the resolver in stack.yaml to whatever you need for your project.
ADD ./config.yaml /root/.stack/config.yaml
ADD ./stack.yaml /root/.stack/global-project/stack.yaml

# Here you'd pull in some Hackage packages you'd need for whatever.
# RUN stack build --system-ghc 'package-name'
